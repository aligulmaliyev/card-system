class BaseService {
  private baseUrl: string;
  constructor(url: string) {
    this.baseUrl = import.meta.env.VITE_BASE_URL + url;
  }

  get = async (url: string) => {
    const response = await fetch(`${this.baseUrl}${url}`, {
      headers: { Authorization: "Bearer " + localStorage.getItem("token") },
    });
    return response.json();
  };
  post = async <T>(url: string, body: T) => {
    const response = await fetch(`${this.baseUrl}${url}`, {
      method: "POST",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });
    return response;
  };

  put = async <T>(url: string, body: T) => {
    const response = await fetch(`${this.baseUrl}${url}`, {
      method: "PUT",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });
    return response;
  };
}

export default BaseService;

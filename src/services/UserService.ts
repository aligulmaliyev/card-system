import { ILogin } from "../models/auth.model";
import BaseService from "./BaseService";

class UserService {
  baseService: BaseService;
  constructor() {
    this.baseService = new BaseService("Auth");
  }
  post = async (user: ILogin) => {
    const response = this.baseService.post("/login", user);
    return response;
  };
  postNewPassword = async (email: string) => {
    const response = this.baseService.post("/new-password", email);
    return response;
  };
}

export default UserService;

import { IPasswords, IProfile } from "../models/profile.model";
import BaseService from "./BaseService";

class ProfileService {
  baseService: BaseService;
  constructor() {
    this.baseService = new BaseService("Profile");
  }
  get = async () => {
    const response = this.baseService.get("");
    return response;
  };

  updateProfile = async (profile: IProfile) => {
    const response = this.baseService.put("", profile);
    return response;
  };

  updatePassword = async (passwords: IPasswords) => {
    const response = this.baseService.put("/password", passwords);
    return response;
  };
}

export default ProfileService;

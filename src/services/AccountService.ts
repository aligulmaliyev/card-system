import { IAccount, IAccountAmount } from "../models/account.model";
import BaseService from "./BaseService";

class AccountService {
  baseService: BaseService;
  constructor() {
    this.baseService = new BaseService("Accounts");
  }
  getList = async () => {
    const response = this.baseService.get("");
    return response;
  };

  post = async (account: IAccount) => {
    const requestModel = {
      id: 0,
      balance: Number(account.balance),
      type: account.type,
    } as IAccount;
    const response = this.baseService.post("", requestModel);
    return response;
  };
  postAmount = async (amount: IAccountAmount) => {
    const requestModel = {
      amount: Number(amount.amount),
    } as IAccountAmount;

    const response = this.baseService.post("", requestModel);
    return response;
  };
}

export default AccountService;

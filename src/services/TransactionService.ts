import BaseService from "./BaseService";

class TransactionsService {
  baseService: BaseService;
  constructor() {
    this.baseService = new BaseService("Transactions");
  }

  getList = async () => {
    const response = this.baseService.get("");
    return response;
  };
}

export default TransactionsService;

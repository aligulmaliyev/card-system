import { ICard } from "../models/card.model";
import BaseService from "./BaseService";

class CardService {
  baseService: BaseService;
  constructor() {
    this.baseService = new BaseService("Cards");
  }

  getList = async () => {
    const response = this.baseService.get("");
    return response;
  };

  get = async (id: number) => {
    const response = this.baseService.get(`/${id}`);
    return response;
  };

  post = async (card: ICard) => {
    const response = this.baseService.post("", card);
    return response;
  };
  put = async (card: ICard) => {
    const response = this.baseService.put("", card);
    return response;
  };
}

export default CardService;

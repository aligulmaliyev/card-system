import { observer } from "mobx-react-lite";
import { Navigate } from "react-router-dom";
import { useStore } from "./store";
import React from "react";

type ProtectedRouteProps = {
  children: JSX.Element;
};

const ProtectedRoute: React.FC<ProtectedRouteProps> = ({ children }) => {
  const { userStore } = useStore();
  if (!userStore.token) {
    return <Navigate to="/login" replace />;
  }

  return children;
};

export default observer(ProtectedRoute);
